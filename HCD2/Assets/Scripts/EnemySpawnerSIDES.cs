﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerSIDES : MonoBehaviour
{
    public GameObject enemy;
    float randY;
    Vector2 spawnPoint;
    public float spawnRate = 2f, spawnDelay = 2f;
    float nextSpawn = 0;
  
    void Update()
    {
      Invoke("SpawnEnemy", spawnDelay);
    }

    void SpawnEnemy()
    {
        if (Time.time > nextSpawn) {
            nextSpawn = Time.time + spawnRate;
            randY = Random.Range(-5, 5);
            spawnPoint = new Vector2(transform.position.x, randY);
            Instantiate(enemy, spawnPoint, Quaternion.identity);
        }
    }
}