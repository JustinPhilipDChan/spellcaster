﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    float randX;
    Vector2 spawnPoint;
    public float spawnRate = 2f, spawnDelay = 2f;
    float nextSpawn = 0;
    void Update()
    {
        Invoke("SpawnEnemy", spawnDelay);   
    }

    void SpawnEnemy()
    {
        if (Time.time > nextSpawn) {
            nextSpawn = Time.time + spawnRate;
            randX = Random.Range(-9, 9);
            spawnPoint = new Vector2(randX, transform.position.y);
            Instantiate(enemy, spawnPoint, Quaternion.identity);
        }
    }
}
