﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public Rigidbody2D playerRB;
    public float movementSpeed = 5f;
    Vector2 playerMovement;
    void Start()
    {
        playerRB = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {  
       playerMovement.x = Input.GetAxisRaw("Horizontal"); 
       playerMovement.y = Input.GetAxisRaw("Vertical");
    }

    void FixedUpdate()
    {
        playerRB.MovePosition(playerRB.position + playerMovement * movementSpeed * Time.fixedDeltaTime);
    }

    //player dies when he collides with enemy
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Enemy"))
        {
            Destroy(col.gameObject);
            this.gameObject.SetActive(false);
            FindObjectOfType<GameManager>().GameOver();
        }
    }
}
