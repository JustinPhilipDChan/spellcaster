using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    public float bulletDamage = 10f;

    public PlayerData playerData;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
    }

     private void OnCollisionEnter2D (Collision2D collider)
    {
        if(collider.gameObject.tag.Equals("Enemy"))
        {
            playerData.ManaKickback(5);
        }

        Destroy(gameObject);
    }
   
}
