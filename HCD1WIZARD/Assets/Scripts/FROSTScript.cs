using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FROSTScript : MonoBehaviour
{
   public float speed = 20f;
    public Rigidbody2D rb;
    public float frostDamage = 15f;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
    }

     private void OnCollisionEnter2D (Collision2D collider)
    {
        Destroy(gameObject);
    }
   
}
