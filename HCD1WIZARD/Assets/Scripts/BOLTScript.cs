using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOLTScript : MonoBehaviour
{
    public float speed = 30f;
    public Rigidbody2D rb;
    public float boltDamage = 25f;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
    }

     private void OnCollisionEnter2D (Collision2D collider)
    {
        Destroy(gameObject);
    }
   
}
