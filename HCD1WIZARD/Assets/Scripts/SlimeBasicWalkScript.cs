using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeBasicWalkScript : MonoBehaviour
{
    public float maxHp = 30;
    public float currentHp = 30;

    public float speed = 10;
    public float distance = 3f;
    private bool goingRight = true;
    public Transform groundDetection;

    //collision damage variables
    [SerializeField] private float lastTouchDamageTime, touchDamageCooldown, touchDamage, touchDamageWidth, touchDamageHeight;
    [SerializeField] private LayerMask whatIsPlayer;
    [SerializeField] private Transform touchDamageCheck;

    private Vector2 touchDamageBotLeft, touchDamageTopRight;
    private float attackDetails;

    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down,distance);

        if (groundInfo.collider == false) {
            if(goingRight) {
                transform.eulerAngles = new Vector3(0, -180, 0);
                goingRight = false;
            } else {
                transform.eulerAngles = new Vector3(0, 0, 0);
                goingRight = true;
            }
        }

       // CheckTouchDamage();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Bullet")) {
            //enemy dies
            if (currentHp <= 0 ) {
                Destroy(gameObject);
            }
            takeDamage(col.gameObject.GetComponent<BulletScript>().bulletDamage);
        }

        // repeating if statements for 3 diff spells
        if (col.gameObject.tag.Equals("Bolt")) {
            //enemy dies
            if (currentHp <= 0 ) {
                Destroy(gameObject);
            }
            takeDamage(col.gameObject.GetComponent<BOLTScript>().boltDamage);
        }
        if (col.gameObject.tag.Equals("Frost")) {
            //enemy dies
            if (currentHp <= 0 ) {
                Destroy(gameObject);
            }
            takeDamage(col.gameObject.GetComponent<FROSTScript>().frostDamage);
        }
        if (col.gameObject.tag.Equals("Flame")) {
            //enemy dies
            if (currentHp <= 0 ) {
                Destroy(gameObject);
            }
            takeDamage(col.gameObject.GetComponent<FLAMEScript>().flameDamage);
        }

        if (col.gameObject.tag.Equals("Player")) {
            CheckTouchDamage();
        }
    }


    private void takeDamage(float dmg)
    {
        if(currentHp >= 0)
        currentHp -= dmg;
        
    }

    private void CheckTouchDamage()
    {
        if (Time.time >= lastTouchDamageTime + touchDamageCooldown) {
            touchDamageBotLeft.Set(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
            touchDamageTopRight.Set(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

            Collider2D hit = Physics2D.OverlapArea(touchDamageBotLeft, touchDamageTopRight, whatIsPlayer);

            if(hit != null) {
                lastTouchDamageTime = Time.time;
                attackDetails = touchDamage;
                hit.SendMessage("Damage", attackDetails);
            }
        }
    }
    //NOT WORKING (supposed to make a box in Unity to show enemy hitbox)
   /* private void OnDrawingGizmos()
    {
        Vector2 botLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
        Vector2 botRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
        Vector2 topRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));
        Vector2 topLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

        Gizmos.DrawLine(botLeft, botRight);
        Gizmos.DrawLine(botRight, topRight);
        Gizmos.DrawLine(topRight, topLeft);
        Gizmos.DrawLine(topLeft, botLeft);
        
    }*/

}
