using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FLAMEScript : MonoBehaviour
{
    public float speed = 15f;
    public Rigidbody2D rb;
    public float flameDamage = 40f;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
    }

     private void OnCollisionEnter2D (Collision2D collider)
    {
        Destroy(gameObject);
    }
   
}
