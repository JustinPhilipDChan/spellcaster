using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    public ControllerScript controller;
    public Joystick joystick;
    public float moveSpeed = 40f;

    float horizontalMove = 0f;
    bool jump = false;

    // Update is called once per frame
    void Update()
    {
        if (joystick.Horizontal >= .1f) {
            horizontalMove = moveSpeed;
        } else if (joystick.Horizontal <= -.1f) {
            horizontalMove = -moveSpeed;
        } else {
            horizontalMove = 0f;
        }

        float verticalMove = joystick.Vertical;

        if (verticalMove >= 0.3f) {
            jump = true;
        }
    }

    void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, jump);
        jump = false;
    }
}
