using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public HpBar hpBar;
    public MpBar mpBar;
    public float maxHealth, currentHealth, invincibilityTimeAfterHurt, maxMana, currentMana;
    private float attackDetails;
    public Rigidbody2D rb;
    public Transform firePoint;
    public GameObject bulletPrefab, boltPrefab, frostPrefab, flamePrefab;
    Renderer rend;
    Color c;

    void Start()
    {
        currentHealth = maxHealth;
        currentMana = maxMana;
        hpBar.SetMaxHealth(maxHealth);
        mpBar.SetMaxMana(maxMana);

        rend = GetComponent<Renderer>();
        c = rend.material.color;
    }

    public void Damage(float attackDetails)
    {
        TakeDamage(attackDetails);
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;

        hpBar.SetHealth(currentHealth);

        if (currentHealth <= 0) {
            //player is dead, IDEALLY instantiate particles before destroying
            Destroy(gameObject);
        } else {
            StartCoroutine(Invulnerable(invincibilityTimeAfterHurt));
        }
    }

    public void ManaKickback(float amount)
    {
        currentMana += amount;
        mpBar.SetMana(currentMana);
    }

    public void ManaUse(float amount)
    {
        currentMana -= amount;
        mpBar.SetMana(currentMana);
    }

    public void Cast(string spellName)
    {
        StartCoroutine(SpellCast(spellName));
    }

    public void Shoot()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }

    IEnumerator SpellCast(string name)
    {
        float castTime = 3f;
        rb.velocity = Vector2.zero;
        if (name == "BOLT")
        {
            ManaUse(20);
            Instantiate(boltPrefab, firePoint.position, firePoint.rotation);
        }
        if (name == "FROST")
        {
            ManaUse(20);
            Instantiate(frostPrefab, firePoint.position, firePoint.rotation);
        }
        if (name == "FLAME")
        {
            ManaUse(20);
            Instantiate(flamePrefab, firePoint.position, firePoint.rotation);
        }
        yield return new WaitForSeconds(castTime);
    }

    IEnumerator Invulnerable(float amount)
    {
        Physics2D.IgnoreLayerCollision(6,7,true);
        c.a = 0.3f;
        rend.material.color = c;
        yield return new WaitForSeconds(amount);
        Physics2D.IgnoreLayerCollision(6,7,false);
        c.a = 1f;
        rend.material.color = c;
    }

}
